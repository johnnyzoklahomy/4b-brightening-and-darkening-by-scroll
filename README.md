let red = 100;
let green = 100;
let blue = 100;


document.body.style.backgroundColor = `rgba(${red}, ${green}, ${blue})`;
// instrukcja if 
const changeColor = (e) => {
    //     if (e.keyCode === 38) {
    //         red += 5;
    //         green += 5;
    //         blue += 5;
    //         document.body.style.backgroundColor = `rgba(${red}, ${green}, ${blue})`;
    //     } else if (e.keyCode === 40) {
    //         red -= 5;
    //         green -= 5;
    //         blue -= 5;
    //         document.body.style.backgroundColor = `rgba(${red}, ${green}, ${blue})`;
    //     }

    // instrukcja switch

    switch (e.keyCode) {

        case 38:
            document.body.style.backgroundColor = `rgba(${red<255 ? red++ : red}, ${green<255 ? green++ : green}, ${blue<255 ? blue++ : blue})`;
            break;

        case 40:
            document.body.style.backgroundColor = `rgba(${red>0 ? red-- : red}, ${green--}, ${blue--})`;
            break;
    }
}
// }


window.addEventListener('keydown', changeColor)